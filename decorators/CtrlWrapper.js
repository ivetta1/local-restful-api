// Function CtrlWrapper takes a controller function (ctrl) as input
const CtrlWrapper = (ctrl) => {
  // Inner function func that will be used as middleware
  const func = async (req, res, next) => {
    try {
      // Execute the provided controller function asynchronously
      await ctrl(req, res, next);
    } catch (err) {
      // If an error occurs during execution of the controller function, pass it to the next middleware
      next(err);
    }
  };
  // Return the middleware function func
  return func;
};

export default CtrlWrapper;
