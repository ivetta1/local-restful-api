import fs from "fs/promises";
import { nanoid } from "nanoid"; // Importing the nanoid function for generating unique IDs

// Directory where data files will be stored
const dataDir = "./data/";

// Controller function to create a new file
export const createFile = async (req, res) => {
  const content = req.body; // Extracting content from the request body
  const filePath = `${dataDir}${nanoid()}.json`; // Generating a unique file name using nanoid and appending .json extension

  await fs.writeFile(filePath, JSON.stringify(content)); // Writing content to the file
  return res.status(201).json({ message: "File created successfully" });
};

// Controller function to fetch all files
export const fetchFiles = async (req, res) => {
  const files = await fs.readdir(dataDir); // Reading all file names from the data directory
  return res.status(200).json(files);
};

// Controller function to fetch a specific file by name
export const fetchFile = async (req, res) => {
  const { fileName } = req.params; // Extracting the fileName parameter from the request URL
  const filePath = `${dataDir}${fileName}.json`; // Constructing the file path

  // Checking if the file exists
  if (!fs.existsSync(filePath)) {
    throw HttpError(404, "File not found");
  }

  const fileContent = await fs.readFile(filePath, "utf8"); // Reading the file content
  res.status(200).json(JSON.parse(fileContent));
};

// Controller function to update a specific file by name
export const updateFile = async (req, res) => {
  const newContent = req.body; // Extracting new content from the request body
  const { fileName } = req.params; // Extracting the fileName parameter from the request URL
  const filePath = `${dataDir}${fileName}.json`; // Constructing the file path

  // Checking if the file exists
  if (!fs.existsSync(filePath)) {
    throw HttpError(404, "File not found");
  }

  await fs.writeFile(filePath, JSON.stringify(newContent)); // Writing the new content to the file
  res.status(200).json({ message: "File updated successfully" });
};

// Controller function to delete a specific file by name
export const deleteFile = async (req, res) => {
  const { fileName } = req.params; // Extracting the fileName parameter from the request URL
  const filePath = `${dataDir}${fileName}.json`; // Constructing the file path

  const isFileExist = await fs.access(filePath); // This will throw an error if the file doesn't exist
  // Checking if the file exists
  if (!fs.existsSync(isFileExist)) {
    throw HttpError(404, "File not found");
  }

  // Deleting the file
  await fs.promises.unlink(isFileExist);
  res.status(200).json({ message: "File deleted successfully" });
};
