import express from "express";
import CtrlWrapper from "../decorators/CtrlWrapper.js";
import { createFile, fetchFiles, fetchFile, updateFile, deleteFile } from "../controllers/file.js";

// Creating a router instance from the Express Router
const fileRouter = express.Router();

fileRouter
  .post("/", CtrlWrapper(createFile)) // POST request to create a new file
  .get("/", CtrlWrapper(fetchFiles)) // GET request to fetch all files
  .get("/:fileName", CtrlWrapper(fetchFile)) // GET request to fetch a specific file
  .put("/:fileName", CtrlWrapper(updateFile)) // PUT request to update a specific file
  .delete("/:fileName", CtrlWrapper(deleteFile)); // DELETE request to delete a specific file

export default fileRouter;
