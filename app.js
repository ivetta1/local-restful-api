import express from "express";
import fileRouter from "./routes/fileRouter.js";

// Defining the port number
const PORT = 3000;

// Creating an Express application instance
const app = express();

// Middleware to parse JSON bodies in the requests
app.use(express.json());


app.use("/api/files", fileRouter);

// Middleware for handling requests to non-existing routes
app.use((req, res) => {
  res.status(404).json({ message: "Service not found" });
});

// Error-handling middleware to catch and handle any errors that occur during request processing
app.use((err, req, res, next) => {
  const { status = 500, message = "Server error" } = err;
  res.status(status).json({ message });
});

// Starting the Express server and listening on the specified port
app.listen(PORT, () => console.log(`Server running on ${PORT} PORT`));
