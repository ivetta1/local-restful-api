const errorMessageList = {
  400: "Bad request (invalid request body)",
  401: "Unauthorized",
  403: "Forbidden",
  404: "Not found",
  409: "Conflict",
};

// Function to create a new HTTP error instance
const HttpError = (status, message = errorMessageList[status]) => {
  const error = new Error(message); // Creating a new Error object with the provided message
  error.status = status; // Adding the HTTP status code to the error object
  return error;
};

export default HttpError;
